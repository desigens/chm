import { initialState, State, Action } from ".";

export function reducer(state: State, action: Action) {
  switch (action.type) {
    case "REVIEWS_REQUEST":
      return {
        ...state,
        reviewsIsFetching: true,
        error: ""
      };
    case "REVIEWS_ERROR":
      return {
        ...state,
        reviewsIsFetching: false,
        error: action.payload
      };
    case "REVIEWS_SUCCESS":
      return {
        ...state,
        reviews: {
          ids: [
            ...state.reviews.ids,
            ...action.payload.reviews.map(({ id }) => id)
          ],
          byIds: {
            ...state.reviews.byIds,
            ...action.payload.reviews.reduce((s, i) => {
              s[i.id] = i;
              return s;
            }, {} as State["reviews"]["byIds"])
          }
        },
        reviewsIsMoreAvailable: action.payload.reviewsIsMoreAvailable,
        reviewsIsFetching: false,
        reviewsFetchedAt: action.payload.reviewsFetchedAt
      };
    case "THEMES_SUCCESS":
      return {
        ...state,
        themes: {
          ids: action.payload.map(({ id }) => id),
          byIds: {
            ...action.payload.reduce((s, i) => {
              s[i.id] = i;
              return s;
            }, {} as State["themes"]["byIds"])
          }
        }
      };
    case "LOGIN_REQUEST":
      return {
        ...state,
        authError: undefined,
        authIsFetching: true
      };
    case "LOGIN_SUCCESS":
      return {
        ...state,
        auth: action.payload,
        authIsFetching: false
      };
    case "LOGIN_ERROR":
      return {
        ...state,
        authError: action.payload,
        authIsFetching: false
      };
    case "FILTER_APPLY":
      return {
        ...state,
        reviews: {
          ...state.reviews,
          reviewsPage: 0,
          ids: []
        },
        reviewsIsFetching: true,
        filter: action.payload,
        error: ""
      };
    case "MORE":
      return {
        ...state,
        reviewsPage: state.reviewsPage + 1
      };
    case "LOGOUT":
      return {
        ...initialState,
        authError: action.payload
      };
    default:
      return state;
  }
}
