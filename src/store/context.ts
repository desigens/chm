import * as React from "react";

import { initialState, State } from "./state";
import { Action } from "./actions";

export const StateContext = React.createContext<
  [State, React.Dispatch<Action>]
>([initialState, () => {}]);
