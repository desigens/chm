import { Theme, Review, Auth } from "../api";
import { State } from "./state";

export type Action =
  | { type: "REVIEWS_REQUEST" }
  | {
      type: "REVIEWS_SUCCESS";
      payload: {
        reviews: Review[];
        reviewsIsMoreAvailable: boolean;
        reviewsFetchedAt: Date;
      };
    }
  | { type: "REVIEWS_ERROR"; payload: string }
  | { type: "THEMES_SUCCESS"; payload: Theme[] }
  | { type: "LOGIN_REQUEST" }
  | { type: "LOGIN_SUCCESS"; payload: Auth }
  | { type: "LOGIN_ERROR"; payload: State["authError"] }
  | { type: "LOGOUT"; payload?: string }
  | { type: "FILTER_APPLY"; payload: State["filter"] }
  | { type: "MORE" };
