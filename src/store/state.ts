import { Theme, Review, Auth } from "../api";

export const initialState: State = {
  auth: {
    expire: "",
    token: "",
    username: ""
  },
  authError: undefined,
  authIsFetching: false,
  reviews: {
    ids: [],
    byIds: {}
  },
  reviewsIsFetching: false,
  reviewsFetchedAt: undefined,
  reviewsIsMoreAvailable: true,
  reviewsPage: 0,
  error: undefined,
  themes: {
    ids: [],
    byIds: {}
  },
  filter: undefined
};

export interface State {
  auth: Auth;
  authError?: string;
  authIsFetching: boolean;
  reviews: {
    ids: number[];
    byIds: { [k: number]: Review | undefined };
  };
  reviewsIsFetching: boolean;
  reviewsFetchedAt?: Date;
  reviewsIsMoreAvailable: boolean;
  reviewsPage: number;
  error?: string;
  themes: {
    ids: number[];
    byIds: { [k: number]: Theme | undefined };
  };
  filter?: Theme["id"];
}
