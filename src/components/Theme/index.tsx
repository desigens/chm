import React from "react";

import { StateContext } from "../../store";
import s from "./styles.module.css";

export const Theme: React.FC<{ id: number; sentiment: number }> = ({
  id,
  sentiment
}) => {
  const [state, dispatch] = React.useContext(StateContext);
  const theme = state.themes.byIds[id];
  const name = theme ? theme.name : id;
  const filtered = state.filter === id;
  return (
    <span
      className={filtered ? s.root_filtered : s.root}
      data-value={sentiment.toString()}
      title={"Filter by" + name}
      onClick={() => {
        if (filtered) return;
        dispatch({
          type: "FILTER_APPLY",
          payload: id
        });
      }}
    >
      {name}
    </span>
  );
};
