import * as React from "react";
import s from "./styles.module.css";

export const Empty: React.FC = () => {
  return (
    <p data-testid="empty" className={s.root}>
      No reviews found
    </p>
  );
};
