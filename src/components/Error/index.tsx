import * as React from "react";
import s from "./styles.module.css";

import { StateContext } from "../../store";

export const Error: React.FC = () => {
  const [state] = React.useContext(StateContext);
  return <p className={s.root}>{state.error}</p>;
};
