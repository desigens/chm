import * as React from "react";

import { Button } from "../Button";
import { login } from "../../api";
import { StateContext } from "../../store";
import s from "./styles.module.css";

export const Form: React.FC = () => {
  const [state, dispatch] = React.useContext(StateContext);
  const [username, setUsername] = React.useState("");
  const [password, setPassword] = React.useState("");

  const onSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    dispatch({ type: "LOGIN_REQUEST" });
    login(username, password).then(
      auth => {
        dispatch({
          type: "LOGIN_SUCCESS",
          payload: auth
        });
        window.localStorage.setItem("auth", JSON.stringify(auth));
      },
      (err: Error) => {
        dispatch({ type: "LOGIN_ERROR", payload: err.message });
      }
    );
  };

  return (
    <div className={s.root}>
      <h1 className={s.header}>Welcome to Chattermill mini</h1>
      <p className={s.text}>Use credentials from your email</p>
      <form data-testid="login" className={s.form} onSubmit={onSubmit}>
        <input
          disabled={state.authIsFetching}
          autoFocus
          className={s.input}
          required
          placeholder="username"
          type="text"
          value={username}
          onChange={e => setUsername(e.target.value)}
        />
        <input
          disabled={state.authIsFetching}
          className={s.input}
          required
          placeholder="password"
          type="password"
          value={password}
          onChange={e => setPassword(e.target.value)}
        />
        <div className={s.button}>
          <Button type="submit" disabled={state.authIsFetching}>
            Log in
          </Button>
        </div>
      </form>
      {state.authError && <p className={s.error}>{state.authError}</p>}
    </div>
  );
};
