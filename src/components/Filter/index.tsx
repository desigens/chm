import * as React from "react";

import { StateContext } from "../../store";
import s from "./styles.module.css";
import { Button } from "../Button";

export const Filter: React.FC = () => {
  const [state, dispatch] = React.useContext(StateContext);

  let filterText = "All themes";
  if (state.filter) {
    const theme = state.themes.byIds[state.filter];
    if (theme) {
      filterText = theme.name;
    }
  }

  return (
    <div className={s.root}>
      <label className={s.label} htmlFor="themeselect">
        Filter reviews:
      </label>
      <div
        data-testid="filter"
        className={state.reviewsIsFetching ? s.select_disabled : s.select}
      >
        <div className={s.select__pretty}>{filterText}</div>
        <select
          id="themeselect"
          data-testid="select"
          disabled={state.reviewsIsFetching}
          className={s.select__native}
          value={(state.filter || "").toString()}
          onChange={e => {
            const value = parseInt(e.target.value);
            dispatch({
              type: "FILTER_APPLY",
              payload: isNaN(value) ? undefined : value
            });
          }}
        >
          <option value="">-- All themes ---</option>
          {state.themes.ids.map(id => {
            const theme = state.themes.byIds[id];
            return theme ? (
              <option key={id} value={theme.id}>
                {theme.name}
              </option>
            ) : null;
          })}
        </select>
        <div className={s.select__focus}></div>
      </div>
      <div className={state.filter ? s.all : s.all_hidden}>
        <Button
          data-testid="all"
          white
          disabled={state.filter === undefined}
          onClick={() => {
            dispatch({
              type: "FILTER_APPLY",
              payload: undefined
            });
          }}
        >
          Show all
        </Button>
      </div>
    </div>
  );
};
