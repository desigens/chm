import * as React from "react";
import {
  render,
  fireEvent,
  waitForElement,
  waitForElementToBeRemoved
} from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

import { App } from ".";

beforeAll(() => {
  jest.spyOn(window, "fetch").mockImplementation(((url: string) => {
    let response: any;
    if (/reviews/.test(url)) {
      const unfilteredReviews = [
        {
          id: 123,
          created_at: "2019-07-18T23:18:53Z",
          comment: "FilterOFF review",
          themes: [{ theme_id: 6374, sentiment: 1 }]
        },
        ...require("../../api/__fixtures__/reviews.json").data
      ];
      const filteredReviews = /theme_id=9000/.test(url)
        ? []
        : [
            {
              id: 456,
              created_at: "2019-07-18T23:18:53Z",
              comment: "FilterON review",
              themes: [{ theme_id: 6374, sentiment: 1 }]
            }
          ];
      response = {
        data: /theme_id=/.test(url) ? filteredReviews : unfilteredReviews
      };
    }
    if (/themes/.test(url)) {
      response = require("../../api/__fixtures__/themes.json");
    }
    if (/login/.test(url)) {
      response = require("../../api/__fixtures__/login.json");
    }
    return Promise.resolve({
      status: 200,
      json() {
        return Promise.resolve(response);
      }
    });
  }) as any);

  jest.spyOn(Storage.prototype, "getItem").mockImplementation(() => {
    return JSON.stringify({
      username: "kitty",
      token: "newtoken",
      expire: "2028-12-08T19:40:25Z"
    });
  });
});

describe("All application", () => {
  test("renders without crashing", () => {
    render(<App />);
  });

  describe("no token", () => {
    beforeEach(() => {
      jest
        .spyOn(Storage.prototype, "getItem")
        .mockImplementationOnce(() => null);
    });

    test("show login form", () => {
      const { getByTestId, queryByTestId } = render(<App />);
      expect(getByTestId("login")).toBeVisible();
      expect(queryByTestId("reviews")).not.toBeInTheDocument();
      expect(queryByTestId("more")).not.toBeInTheDocument();
    });

    test("form hides after authorization", async () => {
      const { getByTestId, queryByTestId, getByPlaceholderText } = render(
        <App />
      );
      fireEvent.change(getByPlaceholderText("username"), {
        target: { value: "user" }
      });
      fireEvent.change(getByPlaceholderText("password"), {
        target: { value: "pass" }
      });
      fireEvent.submit(getByTestId("login"));
      await waitForElementToBeRemoved(() => queryByTestId("login"));
    });
  });

  describe("valid token", () => {
    test("do not show login form, show reviews", async () => {
      const { getByTestId, queryByTestId } = render(<App />);

      expect(queryByTestId("login")).not.toBeInTheDocument();

      await waitForElement(() => getByTestId("reviews"));
      await waitForElement(() => getByTestId("filter"));

      expect(queryByTestId("more")).toBeInTheDocument();
    });

    test("filter loads other reviews, more button hidden", async () => {
      const { getByTestId, getByText, queryByTestId } = render(<App />);

      await waitForElement(() => getByTestId("filter"));
      await waitForElement(() => getByTestId("reviews"));

      expect(getByText(/FilterOFF/)).toBeInTheDocument();
      expect(queryByTestId("more")).toBeInTheDocument();

      fireEvent.change(getByTestId("select"), {
        target: { value: "6335" }
      });

      await waitForElement(() => getByText(/FilterON/));
      expect(queryByTestId("more")).not.toBeInTheDocument();
    });

    test("all reviews button changes select", async () => {
      const { getByTestId } = render(<App />);
      await waitForElement(() => getByTestId("filter"));
      fireEvent.change(getByTestId("select"), {
        target: { value: "6335" }
      });
      expect(getByTestId("select")).toHaveValue("6335");
      fireEvent.click(getByTestId("all"));
      expect(getByTestId("select")).toHaveValue("");
    });

    test("no results", async () => {
      const { getByTestId } = render(<App />);
      await waitForElement(() => getByTestId("filter"));
      fireEvent.change(getByTestId("select"), {
        target: { value: "9000" }
      });
      await waitForElement(() => getByTestId("empty"));
    });
  });

  describe("expired token", () => {
    beforeEach(() => {
      jest.spyOn(Storage.prototype, "getItem").mockImplementationOnce(() =>
        JSON.stringify({
          token: "oldtoken",
          expire: "2018-12-08T19:40:25Z"
        })
      );
    });

    test("show login form with error", async () => {
      const { getByTestId, getByText } = render(<App />);
      await waitForElement(() => getByTestId("login"));
      getByText(/You token is expired/);
    });
  });

  describe("wrong token", () => {
    test("show login form with error", async () => {
      jest.spyOn(window, "fetch").mockImplementationOnce((() => {
        return Promise.resolve({
          status: 401,
          json() {
            return Promise.resolve({ error: true });
          }
        });
      }) as any);
      const { getByTestId, getByText } = render(<App />);
      await waitForElement(() => getByTestId("login"));
      getByText(/You token is broken/);
    });
  });

  describe("logout", () => {
    beforeEach(() => {
      jest.spyOn(window, "confirm").mockImplementationOnce(() => true);
    });

    test("show login form", async () => {
      const { getByTestId } = render(<App />);
      await waitForElement(() => getByTestId("logout"));
      fireEvent.click(getByTestId("logout"));
      await waitForElement(() => getByTestId("login"));
    });

    test("cleans localstorage", async () => {
      const spy = jest.spyOn(Storage.prototype, "removeItem");
      const { getByTestId } = render(<App />);
      await waitForElement(() => getByTestId("logout"));
      fireEvent.click(getByTestId("logout"));
      expect(spy).toHaveBeenCalledWith("auth");
    });
  });
});
