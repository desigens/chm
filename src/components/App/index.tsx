import React, { useEffect, useReducer } from "react";

import s from "./styles.module.css";
import {
  getReviews,
  getThemes,
  ERROR_AUTH_EXPIRED,
  ERROR_AUTH_BROKEN
} from "../../api";
import { reducer, initialState, StateContext } from "../../store";

import { Form } from "../Form";
import { Header } from "../Header";
import { Filter } from "../Filter";
import { Reviews } from "../Reviews";
import { More } from "../More";
import { Empty } from "../Empty";
import { Spinner } from "../Spinner";
import { Error } from "../Error";

/**
 * Get part of initial state from LocalStorage
 */
const useReducerWithLocalStorage = () => {
  const [state, dispatch] = useReducer(reducer, initialState, initialState => {
    const auth = window.localStorage.getItem("auth") || "{}";
    try {
      return {
        ...initialState,
        auth: JSON.parse(auth)
      };
    } catch (err) {
      return initialState;
    }
  });
  return { state, dispatch };
};

export const App: React.FC = () => {
  /**
   * Application store
   */
  const { state, dispatch } = useReducerWithLocalStorage();

  /**
   * Fetch themes and reviews on:
   * - page load
   * - filter applied
   * - more button clicked
   */
  useEffect(() => {
    if (!state.auth.token) return;

    dispatch({ type: "REVIEWS_REQUEST" });
    Promise.all([
      getReviews(state.auth, state.filter, state.reviewsPage).then(reviews => {
        dispatch({ type: "REVIEWS_SUCCESS", payload: reviews });
      }),
      getThemes(state.auth).then(themes => {
        dispatch({ type: "THEMES_SUCCESS", payload: themes });
      })
    ]).catch(err => {
      if ([ERROR_AUTH_EXPIRED, ERROR_AUTH_BROKEN].includes(err.message)) {
        dispatch({ type: "LOGOUT", payload: err.message });
        return;
      }
      dispatch({ type: "REVIEWS_ERROR", payload: err.message });
    });
  }, [state.auth, dispatch, state.filter, state.reviewsPage]);

  /**
   * Remove auth data on logout
   */
  useEffect(() => {
    if (!state.auth.token) {
      window.localStorage.removeItem("auth");
    }
  }, [state.auth]);

  return (
    <StateContext.Provider value={[state, dispatch]}>
      <div data-testid="app">
        {!state.auth.token ? (
          <Form />
        ) : (
          <>
            <Header />
            <div className={s.content}>
              {state.themes.ids.length > 0 && <Filter />}
              {state.reviewsIsFetching && <Spinner />}
              {!state.reviewsIsFetching &&
                state.reviewsFetchedAt &&
                state.reviews.ids.length === 0 && <Empty />}
              {state.error && <Error />}
              {state.reviews.ids.length > 0 && (
                <>
                  <Reviews />
                  <More />
                </>
              )}
            </div>
          </>
        )}
      </div>
    </StateContext.Provider>
  );
};
