import * as React from "react";

import { StateContext } from "../../store";
import { Theme } from "../Theme";
import s from "./styles.module.css";

export const Reviews: React.FC = () => {
  const [state] = React.useContext(StateContext);
  return (
    <div className={s.root}>
      <ul data-testid="reviews" className={s.list}>
        {state.reviews.ids.map(id => (
          <Review id={id} key={id} />
        ))}
      </ul>
    </div>
  );
};

export const Review: React.FC<{
  id: number;
}> = props => {
  const [state] = React.useContext(StateContext);
  const review = state.reviews.byIds[props.id];
  return review ? (
    <li className={s.item}>
      <div>
        <span className={s.date}>{prettyDate(review.created_at)}</span>
        <span className={s.id}>#{props.id}</span>
      </div>
      <div className={s.review}>{review.comment}</div>
      <div>
        {review.themes.map(({ theme_id, sentiment }, index) => {
          return <Theme id={theme_id} sentiment={sentiment} key={index} />;
        })}
      </div>
    </li>
  ) : null;
};

function prettyDate(s: string) {
  return new Date(s).toLocaleDateString("en-EN", {
    year: "numeric",
    month: "long",
    day: "numeric"
  });
}
