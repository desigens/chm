import * as React from "react";

import s from "./styles.module.css";

interface IButtonProps {
  tiny?: boolean;
  white?: boolean;
}

export const Button: React.FC<IButtonProps &
  React.ButtonHTMLAttributes<HTMLButtonElement>> = ({
  tiny,
  white,
  ...otherProps
}) => {
  const cn = [s.button];
  if (tiny) cn.push(s.button_tiny);
  if (white) cn.push(s.button_white);
  return <button className={cn.join(" ")} type="button" {...otherProps} />;
};
