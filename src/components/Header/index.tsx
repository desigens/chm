import * as React from "react";

import { Button } from "../Button";
import { StateContext } from "../../store";
import s from "./styles.module.css";

export const Header: React.FC = () => {
  const [state, dispatch] = React.useContext(StateContext);
  const logout = () => {
    dispatch({ type: "LOGOUT" });
  };
  return (
    <div className={s.root}>
      <h1 className={s.header}>Chattermill mini</h1>
      <div className={s.name}>Hello, {state.auth.username}!</div>
      <Button
        data-testid="logout"
        tiny
        white
        onClick={() => {
          if (window.confirm("Are you sure to logout?")) {
            logout();
          }
        }}
      >
        Logout
      </Button>
    </div>
  );
};
