import * as React from "react";

import s from "./styles.module.css";

/**
 * Spinner animation from https://github.com/tobiasahlin/SpinKit
 */
export const Spinner: React.FC = () => {
  return (
    <div className={s.spinner}>
      <div className={s.bounce1} />
      <div className={s.bounce2} />
    </div>
  );
};
