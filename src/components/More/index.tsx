import * as React from "react";

import { Button } from "../Button";
import { StateContext } from "../../store";
import s from "./styles.module.css";

export const More: React.FC = () => {
  const [state, dispatch] = React.useContext(StateContext);
  return (
    <div className={s.root}>
      {state.reviewsIsMoreAvailable && (
        <Button
          disabled={state.reviewsIsFetching}
          data-testid="more"
          onClick={() => {
            dispatch({ type: "MORE" });
          }}
        >
          {state.reviewsIsFetching ? "Loading..." : "More reviews"}
        </Button>
      )}
    </div>
  );
};
