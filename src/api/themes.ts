import { API_BASE } from "./config";
import { validateAuth, validateResponse, assert, Auth } from "./common";

export interface Theme {
  id: number;
  name: string;
}

export async function getThemes(auth: Auth): Promise<Theme[]> {
  return validateAuth(auth)
    .then(() => get(auth.token))
    .then(validateResponse)
    .then(validateData);
}

function get(token: string) {
  return fetch(API_BASE + "/api/themes?limit=100", {
    method: "GET",
    headers: {
      Authorization: "Bearer " + token
    }
  });
}

function validateData(json: any): Theme[] {
  const data = json.data;
  const guard = (themes: Theme[]): themes is Theme[] => {
    for (let t of themes) {
      assert(typeof t.id, "number");
      assert(typeof t.name, "string");
    }
    return true;
  };
  if (!guard(data)) throw new Error("Invalid data");
  return data;
}
