import { API_BASE, PAGE_SIZE } from "./config";
import { validateResponse, validateAuth, assert, qs, Auth } from "./common";

export interface Review {
  comment: string;
  created_at: string;
  id: number;
  themes: ReviewTheme[];
}

interface ReviewTheme {
  sentiment: number;
  theme_id: number;
}

export async function getReviews(
  auth: Auth,
  theme?: number,
  page = 0
): Promise<{
  reviews: Review[];
  reviewsIsMoreAvailable: boolean;
  reviewsFetchedAt: Date;
}> {
  return validateAuth(auth)
    .then(() => get(auth.token, theme, page * PAGE_SIZE))
    .then(validateResponse)
    .then(validateData)
    .then(reviews => {
      return {
        reviews: reviews.slice(0, PAGE_SIZE),
        reviewsIsMoreAvailable: reviews.length >= PAGE_SIZE,
        reviewsFetchedAt: new Date()
      };
    });
}

function get(token: string, theme_id?: number, offset = 0) {
  const params = { offset, theme_id, limit: PAGE_SIZE + 1 };
  return fetch(API_BASE + "/api/reviews?" + qs(params), {
    method: "GET",
    headers: {
      Authorization: "Bearer " + token
    }
  });
}

function validateData(json: any): Review[] {
  const data = json.data;
  const guard = (reviews: Review[]): reviews is Review[] => {
    for (let r of reviews) {
      assert(typeof r.comment, "string");
      assert(typeof r.created_at, "string");
      assert(typeof r.id, "number");
      assert(Array.isArray(r.themes), true);
      for (let t of r.themes) {
        assert(typeof t.sentiment, "number");
        assert(typeof t.theme_id, "number");
      }
    }
    return true;
  };
  if (!guard(data)) throw new Error("Invalid data");
  return data;
}
