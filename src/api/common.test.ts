import { qs } from "./common";

test("qs", () => {
  expect(qs({ a: 1 })).toBe("a=1");
  expect(qs({ a: 1, b: 2 })).toBe("a=1&b=2");
  expect(qs({ a: 1, b: undefined, c: 3 })).toBe("a=1&c=3");
  expect(qs({ a: 0, b: 2 })).toBe("a=0&b=2");
});
