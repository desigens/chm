import { ERROR_AUTH_BROKEN, ERROR_AUTH_EXPIRED } from "./config";

export interface Auth {
  expire: string;
  token: string;
  username: string;
}

export interface APIError {
  code: number;
  message: string;
}

export function validateAuth(auth: Auth) {
  const expDate = new Date(auth.expire);
  const isExpired = expDate.getTime() - Date.now() < 0;
  return new Promise(resolve => {
    if (isExpired) throw new Error(ERROR_AUTH_EXPIRED);
    resolve();
  });
}

export function validateResponse(response: Response) {
  if (response.status === 401) {
    throw new Error(ERROR_AUTH_BROKEN);
  }
  if (response.status !== 200) {
    throw new Error(response.status.toString());
  }
  return response.json();
}

export function assert(a: any, b: any) {
  if (a !== b) {
    throw new Error("expect '" + a + "' to be equal '" + b + "'");
  }
}

export function qs(params: { [k: string]: any }) {
  return Object.keys(params)
    .filter(k => params[k] !== undefined)
    .map(k => k + "=" + params[k])
    .join("&");
}
