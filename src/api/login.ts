import { API_BASE } from "./config";
import { assert, Auth } from "./common";

interface JWTResponse {
  expire: string;
  token: string;
  code: number;
}

export async function login(username: string, password: string): Promise<Auth> {
  return get(username, password)
    .then(validateResponse)
    .then(validateData)
    .then(({ token, expire }) => ({
      username,
      token,
      expire
    }));
}

function get(username: string, password: string) {
  const formData = new FormData();
  formData.append("username", username);
  formData.append("password", password);
  return fetch(API_BASE + "/login", {
    method: "POST",
    body: formData
  });
}

function validateResponse(response: Response) {
  if (response.status === 401) {
    throw new Error("Login or password is incorrect");
  }
  if (response.status !== 200) {
    throw new Error(response.status.toString());
  }
  return response.json();
}

function validateData(json: any): JWTResponse {
  const guard = (d: any): d is JWTResponse => {
    assert(typeof d.expire, "string");
    assert(typeof d.token, "string");
    return true;
  };
  if (!guard(json)) throw new Error("Invalid data");
  return json;
}
