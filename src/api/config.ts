export const API_BASE = "https://chattermill-challenge.com";
export const PAGE_SIZE = 20;
export const ERROR_AUTH_EXPIRED = "You token is expired, please log in";
export const ERROR_AUTH_BROKEN = "You token is broken, please log in";
