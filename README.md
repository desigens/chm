# Chattermill mini

Simple interface to explore user feedback.

Created with TypeScript, React hooks, and React's useReducer to manage state.

## Run & develop

`npm start`

## Tests

`npm test`

Between unit and e2e tests I prefer to start with covering application
features with as less code as possible. So you can find almost all specs in
`src/components/App/index.test.ts`. Other components left without unit tests
because mostly of their simplicity.

Tests run with Jest and use React Testing Library.

## Production build

`npm run build`

## Features & notes

- If token expires user will see a login form with message about logout reason
- "Load more" button is displayed only when necessary
- Filter component uses native browser select element to provide more a11y

## Known issues

- There is no routing in application
- No unit test in simple components
